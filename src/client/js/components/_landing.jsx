import React, {Component} from 'react'

export default class Landing extends Component {
  render () {
    return <div className="App">
      <header className="Header">
        <div className="Header__logo">
          <img src="/dist/images/logo-fivesteps.png" alt=""/>
        </div>
        <nav className="Header__nav">
          <ul>
            <li><a href="#">HOME</a></li>
            <li><a href="#ourServices">OUR SERVICES</a></li>
            <li><a href="#quotePlan">START NOW</a></li>
          </ul>
        </nav>
      </header>

      <section className="Above">
        <div className="Above__pattern"></div>
        <div className="Above__cont">
          <h1>EXPERT MARKETERS HELPING AUSTRALIAN BUSINESSES SUCCEED</h1>
          <div className="Above__callToAction">
            <a href="#quotePlan">TALK WITH AN EXPERT</a>
          </div>
        </div>
      </section>

      <section className="Description">
        <h2>Our 5 steps to grow your business</h2>
        <div className="Description__lineTitle"></div>
        <div className="Description__cont">
          <div className="Description__stepOne">
            <div className="Description__image">
              <img src="/dist/images/step-one.png" alt=""/>
            </div>
            <div className="Description__content">
              <span>Step 1</span>
              <h3>We’ll catch up with your business and customers</h3>
              <p>We’ll catch up with your business and customers</p>
            </div>
            <img src="/dist/images/description-line-dot1.png" alt="" className="Description__dotLine"/>
          </div>

          <div className="Description__stepTwo">
            <div className="Description__image">
              <img src="/dist/images/step-two.png" alt=""/>
            </div>
            <div className="Description__content">
              <span>Step 2</span>
              <h3>We adapt to your <span>$$</span> situation</h3>
              <p>All budgets and needs are different, our tailormade service will perfectly work with yours.  </p>
            </div>
            <img src="/dist/images/description-line-dot2.png" alt="" className="Description__dotLine"/>
          </div>

          <div className="Description__stepThree">
            <div className="Description__image">
              <img src="/dist/images/step-three.png" alt=""/>
            </div>
            <div className="Description__content">
              <span>Step 3</span>
              <h3>Setting up your tailormade srategy to grow</h3>
              <p>Ok, from now on you won't need to worry about your marketing or bringing clients to your door, let us get in charge and we'll do it for you</p>
            </div>
            <img src="/dist/images/description-line-dot1.png" alt="" className="Description__dotLine"/>
          </div>

          <div className="Description__stepFour">
            <div className="Description__image">
              <img src="/dist/images/step-four.png" width="500" alt=""/>
            </div>
            <div className="Description__content">
              <span>Step 4</span>
              <h3>Start chasing the money</h3>
              <p>Rolling out your campaigns and optimizing them will help us deliver a great return on investment for you.</p>
            </div>
            <img src="/dist/images/description-line-dot2.png" alt="" className="Description__dotLine"/>
          </div>

          <div className="Description__stepFive">
            <div className="Description__image">
              <img src="/dist/images/step-five.png" alt=""/>
            </div>
            <div className="Description__content">
              <span>Step 5</span>
              <h3>Follow up with our expertsto keep growing</h3>
              <p>Delivering results is our top priority, from that point forward we’ll seek for ways to keep your business growing</p>
            </div>

          </div>

        </div>

      </section>

      <section className="Services">
        <a name="ourServices"></a>
        <h2>Our Key Services <span className="Services__lineTitle"></span></h2>
        <div className="Services__cont">
          <div>Google Positioning (SEO & SEM)
            <img src="/dist/images/icon-google-positioning.svg" alt=""/>
          </div>
          <div>Facebook & Instagram Ads
            <img src="/dist/images/icon-facebook-ad.svg" alt=""/>
          </div>
          <div>Google AdSense & AdWords
            <img src="/dist/images/icon-google-adwords.svg" alt=""/>
          </div>
          <div>Twitter Advertisement
            <img src="/dist/images/icon-twitter-ads.svg" alt=""/>
          </div>
          <div>Retargeting
            <img src="/dist/images/icon-retargeting.svg" alt=""/>
          </div>
          <div>A/B Testing
            <img src="/dist/images/icon-ab-testing.svg" alt=""/>
          </div>
          <div>Youtube Ads
            <img src="/dist/images/icon-youtube-ads.svg" alt=""/>
          </div>
          <div>Website design
            <img src="/dist/images/icon-web-design.svg" alt=""/>
          </div>
          <div>Email marketing
            <img src="/dist/images/icon-email-marketing.svg" alt=""/>
          </div>
        </div>
      </section>

      <section className="QuotePlan">
        <div className="QuotePlan__cont">
          <h2>Start growing now</h2>
          <div className="QuotePlan__form">
            <div className="QuotePlan__inputs">
              <div>
                <label htmlFor="">Full name</label>
                <input id="fullName" type="text" placeholder="Elon Musk" required/>
              </div>
              <div>
                <a name="quotePlan"></a>
                <label htmlFor="">Phone number</label>
                <input id="phoneNumber" type="text" placeholder="(+61) 1234567" required/>
              </div>
              <span className="QuotePlan__errorMsg" id="QuotePlan__errorMsg">You must fill all the fields</span>
            </div>
            <button id="formButton">TALK WITH AN EXPERT</button>
          </div>
        </div>
      </section>

      <footer className="Footer">
        <div className="Footer__cont">
          <div>
            <a href=""><span className="icon icon-facebook"></span>FiveSteps</a>
          </div>
          <div>
            <a href=""><span className="icon icon-skype"></span>Skype</a>
          </div>
          <div>
            <a href=""><span className="icon icon-whatsapp"></span>(+61) 0481362400</a>
          </div>
        </div>
      </footer>

    </div>
  }
}

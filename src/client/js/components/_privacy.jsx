import React, {Component} from 'react'

export default class Privacy extends Component{
  render(){
      return <section className="Privacy">
        <header>
          <div className="ThankPage__headerBg"></div>
          <div className="ThankPage__contHeader">
            <div>
              <img src="/dist/images/logo-fivesteps.png" alt="" className="ThankPage__logo"/>
            </div>
            <nav className="ThankPage__nav">
              <div><a href="http://fivesteps.co">Home</a></div>
              <div><a href="http://fivesteps.co/#ourServices">Our Services</a></div>
              <div><a href="http://fivesteps.co/#quotePlan">Begin Now</a></div>
            </nav>
          </div>
        </header>
        <div className="Privacy__content">
          <h1>PRIVACY POLICY</h1>
          <p>At FIVE STEPS, we are committed to protecting the privacy and confidentiality of information about our customers, as well as the data of our customers. We take the following steps to safeguard the privacy of information, both online and offline:</p>
          <ul>
            <li>Notifying customers when we are requesting information from them, including the type of information we are requesting and the purpose for which that information will be used.</li>
            <li>Maintaining accurate records and providing access to personal, identifiable, and retainable information online.</li>
            <li>Using secure system technology, physical and electronic, to safeguard the information regarding our customers.</li>
          </ul>

          <p>
            Our Privacy Policy applies to all information we receive from our customers online and in document form. We collect information from our customers or about our customers in instances when a customer initiates an online request for a demonstration, or executes contracts with FIVE STEPS. In these situations we collect certain information to verify our customers' identities and to verify the customer intent for use of our products or services. The specific information we collect from customers includes: name, address, phone number and email.
          </p>

          <p>
            FIVE STEPS may also use information about our customers for internal business purposes, such as billing, statistics, client services, fraud monitoring, and expanding and improving the products offered by FIVE STEPS. Additionally, we may use the information, including postal address and/or email address, to send you product information and marketing material. (You may opt out of receiving such information by contacting FIVE STEPS and completing a written request to be removed from any such list.)
          </p>

          <h2>Disclosure</h2>

          <p>We may disclose any of the information we collect from or about you (as described above) to the following types of entities upon written request: regulatory authorities; affiliated or partner companies, such as data bureaus; or an entity that has executed a service or product contract with FIVE STEPS for purposes of providing our digital marketing solution(s).</p>

          <p>e do not disclose the personal identity information data you provide to us via our online software to any third party for any marketing purposes. We do not sell, trade, or distribute the information we collect from you to outside vendors or third parties unrelated to the services provided by FIVE STEPS. This type of information may be disclosed to authorized regulatory authorities, upon request, as required by law.</p>

          <h2>Use of Cookies</h2>

          <p>FIVE STEPS uses "cookie" technology in web sessions during which users access our site. The use of cookies, as described above, is limited to the creation of a trace identification number each time you sign on to the website, and such cookies expire within a short period of time after leaving the site.</p>

          <p>Our Privacy Policy may be amended from time to time. Please check this site often to ensure you are accessing the most current version of the policy.</p>

        </div>
        <footer>
          <div>
            <a href="https://www.facebook.com/Five-Steps-1099989990127100/">
              <span className="icon icon-facebook"></span>FiveSteps
            </a>
          </div>
          <div>
            <a href="">
              <span className="icon icon-whatsapp"></span>(+1) 786-347-7583
            </a>
          </div>
        </footer>
      </section>
  }

}

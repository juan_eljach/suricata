import React, {Component} from 'react'

export default class ThankPage extends Component{
  render(){
      return <section className="ThankPage">
        <header>
          <img src="/images/above-bg.png" alt=""/>
          <div className="ThankPage__headerBg"></div>
          <div className="ThankPage__contHeader">
            <div>
              <img src="/images/logo-suricata.png" alt="" className="ThankPage__logo"/>
            </div>
            <nav className="ThankPage__nav">
              <div><a href="#">Home</a></div>
              <div><a href="#">Our Services</a></div>
              <div><a href="#">Begin Now</a></div>
            </nav>
          </div>
        </header>
        <div className="ThankPage__content">
          <h1>THANKS FOR WANTING TO TALK</h1>
          <p>In the next 24 hours one of our experts is going to contact you to start talking about business and how to make it grow :)</p>
          <div>
            <a href="http://fivesteps.co">TAKE ME BACK TO HOME</a>
          </div>
        </div>
        <footer>
          <div>Facebook</div>
          <div>Whatssap</div>
          <div>Skype</div>
        </footer>
      </section>
  }

}

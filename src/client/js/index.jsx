export default $(function(){

  const $fullName = $('#fullName')
  const $phoneNumber = $('#phoneNumber')
  const $formButton = $('#formButton')
  const $errorMsg = $('#QuotePlan__errorMsg')
  const URL = '/registers'

  let pathname = window.location.pathname

  function inputChange(){
    $errorMsg.css({display: 'none'})
  }

  $fullName.keyup(inputChange)
  $phoneNumber.keyup(inputChange)

  $formButton.click((e)=>{
    e.preventDefault()

    let isInputValid = $fullName.val() && $phoneNumber.val()

    if(!isInputValid){
      return $errorMsg.css({display: 'block'})
    }


    const DATA = {fullname: $fullName.val(), phoneNumber: $phoneNumber.val()}

    if(pathname === '/au') DATA.pathName = pathname

    console.log(DATA)

    $.ajax({
      url: URL,
      method: 'POST',
      data: DATA
    })
    .done((d)=> {
      console.log(d)
      $fullName[0].value = ''
      $phoneNumber[0].value = ''
      $errorMsg.css({display: 'none'})
      // window.location.href = "http://fivesteps.co/thanks";
    })

    .fail((f)=> console.log(f))

  })

  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

})

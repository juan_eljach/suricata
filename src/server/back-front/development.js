'use strict'

const express = require('express')
const path = require('path')
const webpack = require('webpack')
const config = require('../../../webpack.config.js')
const Dashboard = require('webpack-dashboard')
const DashboardPlugin = require('webpack-dashboard/plugin')
const app = express()
const compiler = webpack(config)
const dashboard = new Dashboard()
const bodyParser = require('body-parser')


const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/suricata')

const db = mongoose.connection
const Schema = mongoose.Schema

mongoose.Promise = global.Promise;

db.on('error', ()=> console.log('error'))
db.on('open', ()=> console.log('Connected'))

const registerSchema = new Schema({
  fullname: String,
  phone: Number
})

let Register = mongoose.model('Register', registerSchema)

app.use('/dist', express.static(path.resolve('./dist')))

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', false);
    next();
});

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

// Only for development
compiler.apply(new DashboardPlugin(dashboard.setData))
app.use(require('webpack-dev-middleware')(compiler, {
  quiet: true,
  publicPath: config.output.publicPath
}))
app.use(require('webpack-hot-middleware')(compiler, {
  log: () => {}
}))

const html = `<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Grow your business with 5 proven steps</title>
  </head>
  <body>
    <div id="App"></div>

    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
    <script src='dist/app.js'></script>
  </body>
  </html>`

app.get('/', (req, res)=>{
  res.send(html)
})

app.post('/register', (req, res)=> {
  console.log(req.body)

  let register = new Register({
    fullname: req.body.fullname,
    phone: req.body.phoneNumber
  })

  register.save()
    .then(()=>{
      if(err) console.log(err)
      console.log('Saved successfully!')
      res.send('200')
    })
})

app.listen(8080, () => console.log('localhost:8080'))

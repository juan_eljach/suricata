'use strict'

const express = require('express');
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

mongoose.connect('mongodb://localhost/fivesteps')

const db = mongoose.connection;
const Schema = mongoose.Schema

db.on('error', ()=> console.log('Error connecting to db'))
db.on('open', ()=> console.log('DB connected'))

const registerSchema = new Schema({
  fullname: String,
  phone: Number
})

let Register = mongoose.model('Register', registerSchema)
let AuRegister = mongoose.model('AuRegister', registerSchema)

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use('/dist', express.static(path.resolve('./dist')))

app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://fivesteps.co');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', false);
    next();
});

app.set('view engine', 'pug')

app.set('views', path.join(__dirname, 'views'))

app.get('/', (req, res)=> res.render('index', { title: 'Grow your business with 5 proven steps'}))

app.get('/au', (req, res)=> res.render('indexau', { title: 'Grow your business with 5 proven steps'}))

app.get('/thanks', (req, res)=> res.render('thankpage', { title: 'Thank you! We will get in touch very soon'}) )

app.get('/privacy', (req, res)=> res.render('privacy', { title: 'FiveSteps - Grow your business with 5 proven steps'}))

app.post('/registers', (req, res)=> {

  console.log(req.body.pathName)

  if(req.body.pathName) {
    let register_au = new AuRegister({
      fullname: req.body.fullname,
      phone: req.body.phoneNumber
    })

    register_au.save()
      .then(()=>{
        if(err) console.log(err)
    })
    console.log('Saved successfully!')

    return res.status(200).send('200')

  }

  let register = new Register({
    fullname: req.body.fullname,
    phone: req.body.phoneNumber
  })

  register.save()
    .then(()=>{
      if(err) console.log(err)
  })
  console.log('Saved successfully!')
  res.status(200).send('200')

})

app.listen(5000, ()=> console.log('localhost:5000'))

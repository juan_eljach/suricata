module.exports = {
  landing: 'src/client/js/landing.jsx',
  index: 'src/client/js/index.jsx',
  style: 'src/client/scss/landing.scss',
  js: 'src/client/js',
  json: 'src/client/json',
  scss: 'src/client/scss',
  dist: 'dist'
};
